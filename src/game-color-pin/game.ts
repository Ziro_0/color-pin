import 'pixi';
import 'p2';
import * as Phaser from 'phaser-ce';

// import states
import BootState from './states/BootState';
import PreloadState from './states/PreloadState';
import MainMenuState from './states/MainMenuState';
import GameState from './states/GameState';

import IColorPinGameConfig from './IColorPinGameConfig';

/**
 * Main entry game class
 * @export
 * @class Game
 * @extends {Phaser.Game}
 */
export class Game extends Phaser.Game {
    colorPinConfig: IColorPinGameConfig;
    listenerMapping: any = {};
    startLives: number;

    /**
     * Creates an instance of Game.
     * @memberof Game
     */
    constructor(width: number, height: number) {
        // call parent constructor
        super(width, height, Phaser.CANVAS, 'game', null);
        // colorpin design size (480 x 800, portrait)
        // super(480, 800, Phaser.CANVAS, 'game', null);
        console.log('width x height: ', width, height);

        // add some game states
        this.state.add('BootState', new BootState(this));
        this.state.add('PreloadState', new PreloadState(this));
        this.state.add('MainMenuState', new MainMenuState(this));
        this.state.add('GameState', new GameState(this));
    }

    startGame(config: IColorPinGameConfig) {
        console.log('game has started');
        this.colorPinConfig = config;
        console.log(this.colorPinConfig);
        this.state.start('BootState');
    }

    listen(listenValue, cb) {
        this.listenerMapping[listenValue] = cb;
    }

    resurrect() {
    }

    showLivesLost(num) {
    }

    showExtraPoints(num) {
        
    }

    endGame() {
        this.paused = true;
        setTimeout(() => {
            this.destroy();
        }, 100);
    }
}
