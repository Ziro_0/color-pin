import AudioPlayer from '../classes/audio/AudioPlayer';
import LostLivesHeart from '../classes/ui/LostLivesHeart';
import PlayerInputs from '../classes/ui/PlayerInputs';
import UiFlash from '../classes/ui/UiFlash';
import { Game } from '../game';
import Listeners from '../Listeners';
import IColorPinGameConfig, { ILevelData } from '../IColorPinGameConfig';
import DartsLauncher from '../classes/entities/DartsLauncher';
import Target from '../classes/entities/Target';
import Dart from '../classes/entities/Dart';
import LevelBuilder from '../classes/levelBuilder/LevelBuilder';
import LevelText from '../classes/ui/LevelText';
import DesignData from '../classes/data/DesignData';

export default class GameState extends Phaser.State {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _audioPlayer: AudioPlayer;

  private _playerInputs: PlayerInputs;

  private _dartsLauncher: DartsLauncher;

  private _target: Target;

  private _levelText: LevelText;

  private _backgroundGroup: Phaser.Group;

  private _entitiesGroup: Phaser.Group;

  private _uiGroup: Phaser.Group;

  private _incorrectOrientationImage: Phaser.Group;

  private _timer: Phaser.Timer;

  private _playerLevel: number;

  private _displayLevel: number;

  private _hearts: number;

  private _hitStreak: number;

  private _responsiveScale: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    super();
  }

  // ----------------------------------------------------------------------------------------------
  get responsiveScale(): number {
    return (this._responsiveScale);
  }

  // ----------------------------------------------------------------------------------------------
  get audio(): AudioPlayer {
    return (this._audioPlayer);
  }

  // ----------------------------------------------------------------------------------------------
  create() {
    console.log('Game state [create] started');

    this.game.stage.backgroundColor = '#000000';

    const xscale = this.game.width / DesignData.WIDTH;
    const yscale = this.game.height / DesignData.HEIGHT;
    this._responsiveScale = Math.min(xscale, yscale);

    setTimeout(() => {
      this.initOrientation();

      this.initTimer();

      this.initGroups();
      this.initAudioPlayer();
      this.initUi();

      this.startGame();

      this.listenerCallback(Listeners.READY, this.game);
    }, 10);
  }

  // ----------------------------------------------------------------------------------------------
  get gameObject(): Game {
    return (<Game>this.game);
  }

  // ----------------------------------------------------------------------------------------------
  listenerCallback(key: string, ...args: any[]): void {
    const callback: Function = this.gameObject.listenerMapping[key];
    if (callback) {
      callback.call(this.game, ...args);
    }
  }

  // ----------------------------------------------------------------------------------------------
  shutdown(): void {
    this.audio.dispose();
    this._playerInputs.destroy();
    this.game = null;
  }

  // ----------------------------------------------------------------------------------------------
  get timer(): Phaser.Timer {
    return (this._timer);
  }

  // ----------------------------------------------------------------------------------------------
  get uiGroup(): Phaser.Group {
    return (this._uiGroup);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private attachDart(dart: Dart): void {
    this._target.attachDart(dart);

    if (this.handleDartsCollision(dart)) {
      return;
    }

    if (this.handleIncorrectColorPartsMatch(dart)) {
      return;
    }

    this.handleCorrectColorPartHit();

    if (this.isLevelClear()) {
      this.winLevel();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private awardDartHitPoints(): void {
    let points = this.currentLevel.pointsPerHit;
    if (points === undefined) {
      points = 1;
    }

    points *= this._hearts;

    this.listenerCallback(Listeners.ADD_POINT, points, true);
  }

  // ----------------------------------------------------------------------------------------------
  private breakAttachedDart(collidedDart: Dart): void {
    if (collidedDart.isPerma) {
      return;
    }

    collidedDart.repel();
    this._dartsLauncher.loadDarts(collidedDart.color);
  }

  // ----------------------------------------------------------------------------------------------
  private calcViewOffset(): number {
    const headerBarDiv = document.getElementById('headerBar');
    return (headerBarDiv ? headerBarDiv.clientHeight * this.game.scale.scaleFactor.y : 0);
  }

  // ----------------------------------------------------------------------------------------------
  private get config(): IColorPinGameConfig {
    return (this.gameObject.colorPinConfig);
  }

  // ----------------------------------------------------------------------------------------------
  private get currentLevel(): ILevelData {
    return (this.config.levels[this._playerLevel]);
  }

  // ----------------------------------------------------------------------------------------------
  private dartMissed(dart: Dart, collidedDart?: Dart): void {
    this.audio.play('snd-dart-miss');

    dart.repel();

    this._dartsLauncher.loadDarts(dart.color);

    this.resetHearts();

    if (collidedDart) {
      if (this.currentLevel.canBreakExistingDarts) {
        this.breakAttachedDart(collidedDart);
      }
    }

    this.listenerCallback(Listeners.LIVES_LOST, this.game);
  }

  // ----------------------------------------------------------------------------------------------
  private fire(): void {
    if (!this._target.isPresenting) {
      this.audio.play('snd-dart-fire');
      this._dartsLauncher.fire();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private handleCorrectColorPartHit(): void {
    this.audio.play('snd-dart-hit');
    this._target.flashCollidedColorPartImage();
    this.addHitStreak();
    this.awardDartHitPoints();
  }

  // ----------------------------------------------------------------------------------------------
  addHitStreak() {
    let numConsecutiveHitsForHeartsBonus = this.config.numConsecutiveHitsForHeartsBonus;
    if (numConsecutiveHitsForHeartsBonus === undefined) {
      numConsecutiveHitsForHeartsBonus = 15;
    }

    this._hitStreak += 1;

    if (this._hitStreak >= numConsecutiveHitsForHeartsBonus) {
      this.audio.play('snd-heart');
      this.setHearts(this._hearts + 1);
      this._hitStreak = 0;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private handleDartsCollision(dart: Dart): boolean {
    const collidedDart = this._target.getCollidedDart(dart);
    if (!collidedDart) {
      return (false);
    }

    this.dartMissed(dart, collidedDart);
    return (true)
  }

  // ----------------------------------------------------------------------------------------------
  private handleIncorrectColorPartsMatch(dart: Dart): boolean {
    const color = this._target.getHittableColor();
    if (dart.color === color) {
      return (false);
    }

    this.dartMissed(dart);
    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  private hideIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      this._incorrectOrientationImage.destroy();
      this._incorrectOrientationImage = null;
    }

    this.game.paused = false;
  }

  // ----------------------------------------------------------------------------------------------
  private initAudioPlayer(): void {
    this._audioPlayer = new AudioPlayer(this.game);
  }

  // ----------------------------------------------------------------------------------------------
  private initBackground(): void {
    const viewOffset = this.calcViewOffset();
    const image = this.add.image(0, viewOffset, 'background', undefined, this._backgroundGroup);
    image.width = this.game.width;
    image.height = this.game.height;
  }

  // ----------------------------------------------------------------------------------------------
  private initEntitiesDartsLauncher(): void {
    if (this._dartsLauncher) {
      return;
    }

    const yTarget = this._target.yTarget;
    this._dartsLauncher = new DartsLauncher(this.game, yTarget, this._entitiesGroup);

    this._dartsLauncher.fireCompleted.add(this.onDartsLauncherFireCompleted, this);
  }

  // ----------------------------------------------------------------------------------------------
  private initEntitiesTarget(shouldPresent?: boolean): void {
    this._target = new Target(this.game, this.calcViewOffset(), this._entitiesGroup, this._timer);

    if (shouldPresent) {
      this._target.present();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private initOrientation(): void {
    this.scale.onOrientationChange.add((scale: Phaser.ScaleManager,
      prevOrientation: string, wasIncorrect: boolean) => {
        const hasChanged = scale.screenOrientation !== prevOrientation;
        if (!hasChanged) {
          return;
        }

        const isIncorrect = scale.incorrectOrientation && !wasIncorrect;
        if (isIncorrect) {
          this.showIncorrectOrientationMessage();
        } else {
          this.hideIncorrectOrientationMessage();
        }
      });
  }

  // ----------------------------------------------------------------------------------------------
  private initGroup(): Phaser.Group {
    const group = this.game.add.group();
    return (group);
  }

  // ----------------------------------------------------------------------------------------------
  private initGroups(): void {
    this._backgroundGroup = this.initGroup();
    this._entitiesGroup = this.initGroup();
    this._uiGroup = this.initGroup();
  }

  // ----------------------------------------------------------------------------------------------
  private initLevelText(): void {
    this._levelText = new LevelText(this.game, this.calcViewOffset(), this._uiGroup);
  }

  // ----------------------------------------------------------------------------------------------
  private initPlayerInputs(): void {
    this._playerInputs = new PlayerInputs(this.game, this._uiGroup, this.calcViewOffset());
    this._playerInputs.fireSignal.add(() => {
      this.fire();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private initTimer(): void {
    this._timer = this.game.time.create(false);
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private initUi(): void {
    this.initBackground();
    this.initPlayerInputs();
    this.initLevelText();
  }

  // ----------------------------------------------------------------------------------------------
  private isLevelClear(): boolean {
    return (this._dartsLauncher.numDartsLoaded === 0);
  }

  // ----------------------------------------------------------------------------------------------
  private onDartsLauncherFireCompleted(dart: Dart): void {
    this.attachDart(dart);
  }

  // ----------------------------------------------------------------------------------------------
  private presentLostLivesHeart(value: number): void {
    const x = 400;
    const y = 80 + this.calcViewOffset();
    const heart = new LostLivesHeart(this.game, x, y, value);
    this.uiGroup.add(heart);
  }

  // ----------------------------------------------------------------------------------------------
  /*
  private presentScreenFlash(): void {
    if (this.config.isUiImmersive) {
      UiFlash.Create(this.game, 3, this.uiGroup);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private presentScreenTremor(): void {
    if (!this.config.isUiImmersive) {
      return;
    }

    const INTENSITY = 0.02;
    const DURATION = UiFlash.DEFAULT_FLASH_ON_DURATION + UiFlash.DEFAULT_FLASH_OFF_DURATION;
    this.camera.shake(INTENSITY, DURATION);
  }
  */

  // ----------------------------------------------------------------------------------------------
  private resetHearts(): void {
    this._hitStreak = 0;

    if (this._hearts > 1) {
      this.presentLostLivesHeart(this._hearts - 1);
      this.setHearts(1);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private setHearts(hearts: number): void {
    this._hearts = hearts;
    this.listenerCallback(Listeners.SET_HEARTS, this._hearts);
  }

  // ----------------------------------------------------------------------------------------------
  private setLevel(level: number): void {
    this._displayLevel = level;

    const maxLevelIndex = this.config.levels.length - 1;
    this._playerLevel = Math.max(Math.min(maxLevelIndex, this._displayLevel), 0);

    this.initEntitiesTarget(true);
    this.initEntitiesDartsLauncher();

    const levelBuilder = new LevelBuilder(this._dartsLauncher, this._target);
    levelBuilder.build(this.currentLevel);

    this.updateLevelText();

    this.listenerCallback(Listeners.SET_LEVEL, this._displayLevel);
  }

  // ----------------------------------------------------------------------------------------------
  private setStartLevel(): void {
    const level = this.config.startLevel || 0;
    this.setLevel(level);
  }

  // ----------------------------------------------------------------------------------------------
  private showIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      return;
    }

    this.game.paused = true;
    this._incorrectOrientationImage = this.add.group();

    const graphics = this.add.graphics(0, 0, this._incorrectOrientationImage);
    graphics.beginFill(0, 1.0);
    graphics.drawRect(0, 0, this.game.width, this.game.height);
    graphics.endFill();

    const image = this.add.image(0, 0, 'incorrect-orientation-message', undefined,
      this._incorrectOrientationImage);
    image.anchor.set(0.5);
    image.x = this.game.width / 2;
    image.y = this.game.height / 2;

    image.width = this.game.width;
    image.scale.y = image.scale.x;

    if (image.height < this.game.height) {
      image.height = this.game.height
      image.scale.x = image.scale.y;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private startGame(): void {
    this.setHearts(1);
    this._hitStreak = 0;

    this.setStartLevel();
    this.listenerCallback(Listeners.SET_SECONDS,
      this.gameObject.colorPinConfig.startTimeInSeconds);
  }

  // ----------------------------------------------------------------------------------------------
  private updateLevelText(): void {
    this._levelText.setLevel(
      this._displayLevel + 1,
      this._target.getHittableColor()
    );
  }

  // ----------------------------------------------------------------------------------------------
  private async winLevel(): Promise<void> {
    this.audio.play('snd-win-level');

    this._target.win()
      .then(() => {
        this._target.destroy();
        this._target = null;

        this.setLevel(this._displayLevel + 1);
      });
  }
}
