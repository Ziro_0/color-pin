enum GameColors {
  BLACK = 'black',
  BLUE = 'blue',
  CYAN = 'cyan',
  GREEN = 'green',
  MAGENTA = 'magenta',
  PURPLE = 'purple',
  RED = 'red',
  YELLOW = 'yellow',
}

export const GameColorNames = new Map<GameColors, number>([
  [ GameColors.BLACK, 0x000000 ],
  [ GameColors.BLUE, 0x0000ff ],
  [ GameColors.CYAN, 0x00ffff ],
  [ GameColors.GREEN, 0x00ff00 ],
  [ GameColors.MAGENTA, 0xff00ff ],
  [ GameColors.PURPLE, 0x7f00ff ],
  [ GameColors.RED, 0xff0000 ],
  [ GameColors.YELLOW, 0xffdf00 ],
]);

export default GameColors;
