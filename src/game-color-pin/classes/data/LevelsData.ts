import { ILevelData, PartSizeConfigurations } from '../../IColorPinGameConfig';
import GameColors from './GameColors';

/**
 * Array of data blocks, each block describes a level.
 */
export const LEVELS_DATA: ILevelData[] = [
  // level 1
  {
    numDartsPerColor: 1,
    partSizeConfiguration: PartSizeConfigurations.TWO_HALVES,
    stemLength: 55,
    canBreakExistingDarts: false,
    targetMotions: [
      {
        isClockWise: true,
        speed: 3500,
      },
    ],
  },

  // level 2
  {
    numDartsPerColor: 1,
    partSizeConfiguration: PartSizeConfigurations.THREE_THIRDS,
    stemLength: 55,
    canBreakExistingDarts: false,
    targetMotions: [
      {
        isClockWise: true,
        speed: 3500,
      },
    ],
  },

  // level 3
  {
    numDartsPerColor: 1,
    partSizeConfiguration: PartSizeConfigurations.FOUR_QUARTERS,
    stemLength: 50,
    canBreakExistingDarts: false,
    targetMotions: [
      {
        isClockWise: true,
        speed: 3500,
      },
    ],
  },

  // level 4
  {
    partColors: [GameColors.RED, GameColors.GREEN, GameColors.BLUE],
    numDartsPerColor: 2,
    partSizeConfiguration: PartSizeConfigurations.ONF_HALF_TWO_QUARTERS,
    stemLength: 50,
    canBreakExistingDarts: false,
    targetMotions: [
      {
        isClockWise: true,
        speed: 3500,
      },
    ],
  },

  // level 5
  {
    numDartsPerColor: 2,
    partSizeConfiguration: PartSizeConfigurations.TWO_HALVES,
    permaDartAngles: [0, 180],
    stemLength: 45,
    canBreakExistingDarts: false,
    targetMotions: [
      {
        isClockWise: false,
        speed: 3500,
      },
    ],
  },

  // level 6
  {
    numDartsPerColor: 2,
    partSizeConfiguration: PartSizeConfigurations.THREE_THIRDS,
    permaDartAngles: [30, 150, 270],
    stemLength: 45,
    canBreakExistingDarts: false,
    targetMotions: [
      {
        isClockWise: false,
        speed: 3500,
      },
    ],
  },

  // level 7
  {
    numDartsPerColor: 3,
    partSizeConfiguration: PartSizeConfigurations.FOUR_QUARTERS,
    permaDartAngles: [0, 90, 180, 270],
    stemLength: 60,
    canBreakExistingDarts: false,
    targetMotions: [
      {
        isClockWise: false,
        speed: 6000,
      },
    ],
  },

  // level 8
  {
    numDartsPerColor: 3,
    partSizeConfiguration: PartSizeConfigurations.TWO_HALVES,
    permaDartAngles: [0, 45, 90, 135, 180, 225, 270, 315],
    stemLength: 45,
    canBreakExistingDarts: false,
    targetMotions: [
      {
        isClockWise: false,
        speed: 3000,
        durationMs: 5000,
      },
      {
        isClockWise: true,
        speed: 3000,
        durationMs: 5000,
      },
    ],
  },

  // level 9
  {
    numDartsPerColor: 3,
    partSizeConfiguration: PartSizeConfigurations.ONF_HALF_TWO_QUARTERS,
    permaDartAngles: [20, 40, 140, 160, 260, 280],
    stemLength: 70,
    canBreakExistingDarts: true,
    targetMotions: [
      {
        isClockWise: false,
        speed: 4000,
        durationMs: 5000,
      },
      {
        isClockWise: false,
        speed: 2000,
        durationMs: 3000,
      },
      {
        isClockWise: true,
        speed: 6000,
        durationMs: 8000,
      },
      {
        isClockWise: false,
        speed: 500,
        durationMs: 1000,
      },
      {
        isClockWise: true,
        speed: 3500,
        durationMs: 4000,
      },
    ],
  },
];
