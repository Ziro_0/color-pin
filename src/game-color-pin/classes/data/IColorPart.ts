import GameColors from './GameColors';

export enum ColorPartSizes {
  HALF = 2,
  THIRD = 3,
  QUARTER = 4,
}

interface IColorPart {
  size: ColorPartSizes;
  angle: number;
  color?: GameColors;
}

export default IColorPart;
