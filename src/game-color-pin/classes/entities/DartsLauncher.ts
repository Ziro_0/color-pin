import GameState from '../../states/GameState';
import GameColors from '../data/GameColors';
import Dart from './Dart';

export default class DartsLauncher extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly TEXTURE_KEY = 'darts-launcher';

  private static readonly DARTS_Y_START_OFFSET = 56;

  private static readonly DARTS_Y_ADD_OFFSET = 40;

  fireCompleted: Phaser.Signal;

  stemLength: number;

  private _darts: Dart[];

  private _yFireTarget: number;

  private _fireDuration: number;

  private _responsiveScale: number;

  private _isFiring: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, yFireTarget: number, group: Phaser.Group) {
    super(game, 0, 0, DartsLauncher.TEXTURE_KEY);

    this.fireCompleted = new Phaser.Signal();

    this._responsiveScale = (<GameState> this.game.state.getCurrentState()).responsiveScale;
    this.width *= this._responsiveScale;
    this.height *= this._responsiveScale;

    this.x = this.game.width / 2;
    this.y = this.game.height - this.height;

    this._yFireTarget = yFireTarget;

    group.add(this);

    this.anchor.set(0.5, 0);

    this._darts = [];
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.fireCompleted.dispose();
    super.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  fire(): boolean {
    if (this._isFiring) {
      return (false);
    }

    if (!this.fireDart()) {
      return (false); //sanity check
    }

    this._isFiring = true;

    this.prepNextDart();

    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  set fireDuration(value: number) {
    this._fireDuration = value;
  }

  // ----------------------------------------------------------------------------------------------
  loadDarts(colors: GameColors | GameColors[]): void {
    const colorsArray = Array.isArray(colors) ? colors : [colors];
    colorsArray.forEach((color) => {
      this.loadDart(color);
    });
  }

  // ----------------------------------------------------------------------------------------------
  get numDartsLoaded(): number {
    return (this._darts.length);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private fireDart(): boolean {
    const dart = this._darts.shift();
    if (!dart) {
      return (false); //sanity check
    }

    if (this.stemLength > 0) {
      dart.stemLength = this.stemLength;
    }

    dart.fire(this._yFireTarget, this._fireDuration);

    dart.fireCompleted.addOnce(() => {
      this._isFiring = false;
      this.fireCompleted.dispatch(dart, this);
    });

    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  private loadDart(color: GameColors): void {
    const dart = new Dart(this.game, color, <Phaser.Group> this.parent);

    dart.width *= this._responsiveScale;
    dart.scale.y = dart.scale.x;

    dart.x = this.game.width / 2;

    const yStart = this.y + (DartsLauncher.DARTS_Y_START_OFFSET * this._responsiveScale);

    dart.y = yStart + (this._darts.length * DartsLauncher.DARTS_Y_ADD_OFFSET) * this._responsiveScale;

    this._darts.push(dart);
  }

  // ----------------------------------------------------------------------------------------------
  private prepNextDart(): void {
    const yStart = this.y + (DartsLauncher.DARTS_Y_START_OFFSET * this._responsiveScale);

    const ofs = DartsLauncher.DARTS_Y_ADD_OFFSET * this._responsiveScale;
    this._darts.forEach((dart, index) => {
      this.game.tweens.create(dart)
        .to(
          {
            y: yStart + index * ofs,
          },
          this._fireDuration,
          Phaser.Easing.Linear.None,
          true,
        );
    });
  }
}