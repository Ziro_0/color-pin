import { ThrowStmt } from '@angular/compiler';
import { ITargetMotionCycle } from '../../IColorPinGameConfig';
import GameState from '../../states/GameState';
import GameColors, { GameColorNames } from '../data/GameColors';
import IColorPart, { ColorPartSizes } from '../data/IColorPart';
import Dart from './Dart';

// ================================================================================================
interface IColorPartImage {
  image: Phaser.Image;
  color?: GameColors;
  visibleStartOffsetRadians?: number;
  visibleEndOffsetRadians?: number;
}

export default class Target extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly X = 240;

  private static readonly Y = 260;

  private static readonly Y_TARGET_OFFSET = 24;

  private static readonly WIN_TWEEN_DURATION = 200;

  private static readonly PRESENT_TWEEN_DURATION = 300;

  private static readonly WIN_TWEEN_SCALE = 1.25;

  permaDartStemLength: number;

  private _colorPartImages: IColorPartImage[];

  private _targetMotions: ITargetMotionCycle[];

  private _circle: Phaser.Image;

  private _spinTween: Phaser.Tween;

  private _gameTimer: Phaser.Timer;

  private _motionCycleTimerEvent: Phaser.TimerEvent;

  private _responsiveScale: number;

  private _isPresenting: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, viewOffset: number, parent: Phaser.Group,
    gameTimer: Phaser.Timer) {
    super(game, parent);

    this._gameTimer = gameTimer;

    this.initVisuals();

    this.width = this.game.width / 2;
    this.scale.y = this.scale.x;

    this._responsiveScale = (<GameState> this.game.state.getCurrentState()).responsiveScale;
    this.x = this.game.width / 2;
    this.y = Target.Y * this._responsiveScale +  viewOffset;

    this._colorPartImages = [];
  }

  // ----------------------------------------------------------------------------------------------
  angleBetween(x: number, y: number): number {
    let radians = Phaser.Math.angleBetween(this.x, this.y, x, y);
    if (radians < 0) {
      radians = -radians;
    } else if (radians > 0) {
      radians = 2 * Math.PI - radians;
    }

    return (radians);
  }

  // ----------------------------------------------------------------------------------------------
  attachDart(dart: Dart): void {
    dart.scale.set(1.0);
    this.setDartPositionToLocal(dart);
    dart.rotation = Dart.RADIANS_OFFSET - this.rotation;
    this.add(dart);
  }

  // ----------------------------------------------------------------------------------------------
  attachPermaDart(dartAngle: number): void {
    const dart = new Dart(this._circle.game, GameColors.BLACK, this);
    dart.isPerma = true;

    if (this.permaDartStemLength > 0) {
      dart.stemLength = this.permaDartStemLength;
    }

    dart.setFiringDisposition();

    const rotation = Phaser.Math.degToRad(this.angle - dartAngle);
    const distance = this.yTargetDistance;
    dart.x = Math.cos(rotation) * distance;
    dart.y = Math.sin(rotation) * distance;

    dart.rotation += rotation - Math.PI / 2;
    this.add(dart);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.stopMotion();
    this.stopSpin();
    this._gameTimer = null;
    super.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  flashCollidedColorPartImage(): void {
    const cpi = this.getHittableColorPartImage();
    if (!cpi) {
      return;
    }

    cpi.image.tint = 0xffffff;
    this._gameTimer.add(80, () => {
      cpi.image.tint = -1;
    });
  }

  // ----------------------------------------------------------------------------------------------
  getHittableColor(): GameColors {
    const cpi = this.getHittableColorPartImage();
    return (cpi ? cpi.color : null);
  }

  // ----------------------------------------------------------------------------------------------
  getHittableColorPartImage(): IColorPartImage {
    const wrap = Target.NormalizeDegrees;
    const TARGET_DEGREES = 90;

    const target = this._colorPartImages.find((cpi) => {
      const startRadians = wrap(this.angle + cpi.visibleStartOffsetRadians);
      const endRadians = wrap(this.angle + cpi.visibleEndOffsetRadians);

      if (endRadians < startRadians) {
        return (endRadians <= TARGET_DEGREES && TARGET_DEGREES < startRadians);
      }

      return (startRadians > TARGET_DEGREES);
    });

    return (target);
  }

  // ----------------------------------------------------------------------------------------------
  getCollidedDart(dart: Dart): Dart {
    const collidedDart = <Dart> this.children.find((displayObject) => {
      if (dart === displayObject) {
        return (false);
      }

      const testDart = <Dart> displayObject;
      if (! (testDart instanceof Dart)) {
        return (false);
      }

      return (dart.collidesWithDart(testDart));
    });

    return (collidedDart);
  }

  // ----------------------------------------------------------------------------------------------
  get isPresenting(): boolean {
    return (this._isPresenting);
  }

  // ----------------------------------------------------------------------------------------------
  present(): Promise<void> {
    this.alpha = 0;

    this._isPresenting = true;

    return (new Promise((resolve) => {
      this.game.tweens.create(this)
        .to(
          {
            alpha: 1.0,
          },
          Target.PRESENT_TWEEN_DURATION,
          Phaser.Easing.Cubic.In,
          true
        )
        .onComplete.addOnce(() => {
          this._isPresenting = false;
          resolve();
        });
      }
    ));
  }

  // ----------------------------------------------------------------------------------------------
  repelDart(dart: Dart): void {
    this.setDartPositionToWorld(dart);

    dart.rotation = Dart.RADIANS_OFFSET;

    this.remove(dart);
    this.game.add.existing(dart);

    dart.repel();
  }

  // ----------------------------------------------------------------------------------------------
  setColorParts(colorParts: IColorPart[]): boolean {
    this.removeColorParts();

    colorParts.forEach((colorPart) => {
      this.addColorPart(colorPart);
    });

    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  setMotion(targetMotions: ITargetMotionCycle[]): void {
    this._targetMotions = targetMotions;

    if (this._targetMotions) {
      this.startNextMotionCycle();
    } else {
      console.warn('no target motions specified');
    }
  }

  // ----------------------------------------------------------------------------------------------
  get yTarget(): number {
    return (this.centerY + (this._circle.bottom - Target.Y_TARGET_OFFSET) * this._responsiveScale);
  }

  // ----------------------------------------------------------------------------------------------
  get yTargetDistance(): number {
    return (this._circle.bottom - Target.Y_TARGET_OFFSET);
  }

  // ----------------------------------------------------------------------------------------------
  win(): Promise<void> {
    const scale = Target.WIN_TWEEN_SCALE * this._responsiveScale;
    return (new Promise((resolve) => {
      this.game.tweens.create(this)
        .to(
          {
            alpha: 0,
          },
          Target.WIN_TWEEN_DURATION,
          Phaser.Easing.Linear.None,
          true
        );

      this.game.tweens.create(this.scale)
        .to(
          {
            x: scale,
            y: scale,
          },
          Target.WIN_TWEEN_DURATION,
          Phaser.Easing.Linear.None,
          true
        )
        .onComplete.addOnce(() => {
          resolve();
        });
      }
    ));
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private addColorPart(colorPart: IColorPart): void {
    const colorPartImage = <IColorPartImage> {
      image: this.createColorPartImage(colorPart.size),
    };

    this.setColorPartColor(colorPartImage, colorPart.color);

    colorPartImage.image.angle = colorPart.angle;

    Target.SetVisibleOffsetRadians(colorPart.size, colorPartImage);
    colorPartImage.visibleStartOffsetRadians += colorPart.angle;
    colorPartImage.visibleEndOffsetRadians += colorPart.angle;

    this._colorPartImages.push(colorPartImage);
  }

  // ----------------------------------------------------------------------------------------------
  private createColorPartImage(size: ColorPartSizes): Phaser.Image {
    let textureKey: string;

    if (size === ColorPartSizes.HALF) {
      textureKey = 'color-parts-half';
    } else if (size === ColorPartSizes.THIRD) {
      textureKey = 'color-parts-third';
    } else if (size === ColorPartSizes.QUARTER) {
      textureKey = 'color-parts-quarter';
    }

    return (this.initImage(textureKey));
  }

  // ----------------------------------------------------------------------------------------------
  private getNextMotionCycle(): ITargetMotionCycle {
    const motionCycle = this._targetMotions.shift();
    if (!motionCycle) {
      return (null);
    }

    this._targetMotions.push(motionCycle);
    return (motionCycle);
  }

  // ----------------------------------------------------------------------------------------------
  private static GetRandomColor(): GameColors {
    const colors = Array.from(GameColorNames.keys());
    return (Phaser.ArrayUtils.getRandomItem(colors));
  }

  // ----------------------------------------------------------------------------------------------
  private initImage(textureKey: string): Phaser.Image {
    const image = this.game.add.image(0, 0, textureKey, undefined, this);
    image.anchor.set(0.5);
    return (image);
  }

  // ----------------------------------------------------------------------------------------------
  private initVisuals(): void {
    this.initImage('circle-bg');
    this._circle = this.initImage('circle');
  }

  // ----------------------------------------------------------------------------------------------
  private static NormalizeDegrees(degrees: number): number {
    return (Phaser.Math.wrap(degrees, 0, 360));
  }

  // ----------------------------------------------------------------------------------------------
  private removeColorParts(): void {
    this._colorPartImages.forEach((colorPartImage) => {
      colorPartImage.image.destroy();
    });

    this._colorPartImages.length = 0;
  }

  // ----------------------------------------------------------------------------------------------
  private setColorPartColor(cpi: IColorPartImage, color: GameColors): void {
    if (color === undefined) {
      color = Target.GetRandomColor();
    }

    cpi.color = color;
    cpi.image.tint = GameColorNames.get(color);
  }

  // ----------------------------------------------------------------------------------------------
  private setDartPositionToLocal(dart: Dart): void {
    const dartStartPoint = new PIXI.Point(0, 0);
    const dartGlobalPoint = dart.toGlobal(dartStartPoint);
    const dartLocalPoint = this.toLocal(dartGlobalPoint, undefined);
    dart.x = dartLocalPoint.x;
    dart.y = dartLocalPoint.y;
  }

  // ----------------------------------------------------------------------------------------------
  private setDartPositionToWorld(dart: Dart): void {
    const dartStartPoint = new PIXI.Point(0, 0);
    const dartGlobalPoint = dart.toGlobal(dartStartPoint);
    dart.x = dartGlobalPoint.x;
    dart.y = dartGlobalPoint.y;
  }

  // ----------------------------------------------------------------------------------------------
  private static SetVisibleOffsetRadians(size: ColorPartSizes,
    colorPartImageOut: IColorPartImage): void {
    if (size === ColorPartSizes.HALF) {
      colorPartImageOut.visibleStartOffsetRadians = 0;
      colorPartImageOut.visibleEndOffsetRadians = 180;
    } else if (size === ColorPartSizes.THIRD) {
      colorPartImageOut.visibleStartOffsetRadians = 330;
      colorPartImageOut.visibleEndOffsetRadians = 210;
    } else if (size === ColorPartSizes.QUARTER) {
      colorPartImageOut.visibleStartOffsetRadians = 315;
      colorPartImageOut.visibleEndOffsetRadians = 225;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private spin(isClockwise: boolean, durationMs: number): void {
    this.stopSpin();

    this._spinTween = this.game.tweens.create(this)
      .to(
        {
          angle: this.angle + (isClockwise ? 360 : -360),
        },
        durationMs,
        Phaser.Easing.Linear.None,
        true,
      )
      .loop(true);
  }

  // ----------------------------------------------------------------------------------------------
  private startNextMotionCycle(): void {
    const motionCycle = this.getNextMotionCycle();
    if (!motionCycle) {
      console.warn('no target motion cycle found');
      return;
    }

    this.spin(motionCycle.isClockWise, motionCycle.speed);

    this.stopMotion();

    this._motionCycleTimerEvent = this._gameTimer.add(motionCycle.durationMs, () => {
      this.startNextMotionCycle();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private stopMotion(): void {
    if (this._motionCycleTimerEvent) {
      this._gameTimer.remove(this._motionCycleTimerEvent);
      this._motionCycleTimerEvent = null;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private stopSpin(): void {
    if (this._spinTween) {
      this._spinTween.stop();
      this._spinTween = null;
    }
  }
}
