import GameState from '../../states/GameState';
import GameColors from '../data/GameColors';
import Ball from './Ball';

export default class Dart extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static readonly RADIANS_OFFSET = Math.PI / 2;

  private static readonly REPEL_DURATION_MS = [200, 300];

  private static readonly REPEL_RADIANS = [Math.PI / 4, 3 * Math.PI / 4];

  private static readonly REPEL_DISTANCE = 150;

  private static readonly REPEL_SPIN_DURATION_MS = [175, 500];

  fireCompleted: Phaser.Signal;

  isPerma: boolean;

  private _ball: Ball;

  private _stem: Phaser.Image;

  private _repelMoveTween: Phaser.Tween;

  private _repelSpinTween: Phaser.Tween;

  private _responsiveScale: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, ballColor: GameColors, parent: Phaser.Group) {
    super(game, parent);

    this._responsiveScale = (<GameState> this.game.state.getCurrentState()).responsiveScale;

    this.fireCompleted = new Phaser.Signal();

    this.initStem();
    this.initBall(ballColor);
  }

  // ----------------------------------------------------------------------------------------------
  collidesWithDart(targetDart: Dart): boolean {
    if (!targetDart) {
      return (false);
    }

    const thisBallCircle = this.ballCircle;
    const targetBallCircle = targetDart.ballCircle;
    return (Phaser.Circle.intersects(thisBallCircle, targetBallCircle));
  }

  // ----------------------------------------------------------------------------------------------
  get color(): GameColors {
    return (this._ball.color);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    if (this._repelMoveTween) {
      this._repelMoveTween.stop();
      this._repelMoveTween = null;
    }

    if (this._repelSpinTween) {
      this._repelSpinTween.stop();
      this._repelSpinTween = null;
    }

    this.fireCompleted.dispose();
    super.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  fire(yTarget: number, durationMs: number): void {
    this.setFiringDisposition();

    const tween = this.game.tweens.create(this)
      .to(
        {
          y: yTarget,
        },

        durationMs,
        Phaser.Easing.Linear.None,
        true,
      );

    tween.onComplete.addOnce(() => {
      this.fireCompleted.dispatch(this);
    });
  }

  // ----------------------------------------------------------------------------------------------
  repel(): void {
    if (this._repelMoveTween) {
      return;
    }

    this.adjustAnchorForRepelSpin();

    this.createRepelMoveTween();
    this.createRepelSpinTween();
  }

  // ----------------------------------------------------------------------------------------------
  setFiringDisposition(): void {
    this.setStemMode();
    this.rotation = Dart.RADIANS_OFFSET;
    this.y -= this._stem.width;
    this._ball.rotation -= Dart.RADIANS_OFFSET;
  }

  // ----------------------------------------------------------------------------------------------
  set stemLength(value: number) {
    this._stem.width = value;
    this.updateBallPositionFromStem();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private adjustAnchorForRepelSpin(): void {
    const prevRotation = this.rotation;
    this.rotation = 0;
    this._stem.x = -this._stem.width / 2;
    this.updateBallPositionFromStem();
    this.rotation = prevRotation;
  }

  // ----------------------------------------------------------------------------------------------
  private get ballCircle(): Phaser.Circle {
    const worldPoint = this.ballWorldPoint;
    return (new Phaser.Circle(worldPoint.x, worldPoint.y, this._ball.width));
  }

  // ----------------------------------------------------------------------------------------------
  private get ballWorldPoint(): PIXI.Point {
    const dartStartPoint = new PIXI.Point(this._ball.x, this._ball.y);
    return (this.toGlobal(dartStartPoint));
  }

  // ----------------------------------------------------------------------------------------------
  private createRepelMoveTween(): void {
    const radians = Phaser.Math.random(Dart.REPEL_RADIANS[0], Dart.REPEL_RADIANS[1]);
    const xTarget = Math.cos(radians) * Dart.REPEL_DISTANCE;
    const yTarget = Math.sin(radians) * Dart.REPEL_DISTANCE;

    this._repelMoveTween = this.game.tweens.create(this)
      .to(
        {
          x: this.x + xTarget,
          y: this.y + yTarget,
          alpha: 0,
        },

        Phaser.Math.random(Dart.REPEL_DURATION_MS[0], Dart.REPEL_DURATION_MS[1]),
        Phaser.Easing.Linear.None,
        true,
      );

    this._repelMoveTween.onComplete.addOnce(() => {
      this.destroy();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private createRepelSpinTween(): void {
    this._repelSpinTween = this.game.tweens.create(this)
      .to(
        {
          rotation: this.rotation + (2 * Math.PI * (Math.random() >= 0.5 ? 1 : -1)),
        },

        Phaser.Math.random(Dart.REPEL_SPIN_DURATION_MS[0], Dart.REPEL_SPIN_DURATION_MS[1]),
        Phaser.Easing.Linear.None,
        true,
      ).loop(true);
  }

  // ----------------------------------------------------------------------------------------------
  private initBall(ballColor: GameColors): void {
    this._ball = new Ball(this.game, ballColor);
    this.add(this._ball);
  }

  // ----------------------------------------------------------------------------------------------
  private initStem(): void {
    this._stem = this.game.add.image(0, 0, 'dart-stem');
    this._stem.visible = false;
    this.add(this._stem);
  }

  // ----------------------------------------------------------------------------------------------
  private setStemMode(): void {
    this.updateBallPositionFromStem();
    this._stem.visible = true;
  }

  // ----------------------------------------------------------------------------------------------
  private updateBallPositionFromStem(): void {
    this._ball.x = this._stem.width;
  }
}