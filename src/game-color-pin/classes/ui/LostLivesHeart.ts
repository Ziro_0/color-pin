export default class LostLivesHeart extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, lives: number) {
    super(game, x, y, 'lost-lives-heart');

    this.anchor.setTo(0.5);
    this.scale.setTo(0.5);

    const text = this.game.add.text(0, -60, lives.toString(10));
    text.anchor.setTo(0.5);
    text.fontSize = 60;
    text.fill = '#e75a70';
    this.addChild(text);

    this.game.add
      .tween(this.scale)
      .to({ x: 1, y: 1 }, 250, 'Bounce', true)
      .onComplete.add(() => {
        this.game.add
          .tween(this)
          .to({ y: '-150', alpha: 0 }, 500, 'Linear', true)
          .onComplete.add(() => {
            this.destroy();
          });
      });
  }
}
