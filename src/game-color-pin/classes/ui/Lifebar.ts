export default class Lifebar extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _fill: Phaser.Image;

  private _fillMask: Phaser.Graphics;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, parent: Phaser.Group) {
    super(game, parent);
    this.init();
  }

  // ----------------------------------------------------------------------------------------------
  set tint(value: number) {
    this._fill.tint = value;
  }

  // ----------------------------------------------------------------------------------------------
  get value(): number {
    return (this._fillMask.scale.x);
  }

  // ----------------------------------------------------------------------------------------------
  set value(x: number) {
    if (x === 0) {
      x = 0.00001;
    }

    this._fillMask.scale.x = x;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private init(): void {
    this.game.add.image(0, 0, 'lifebar-frame', undefined, this);

    this._fill = this.game.add.image(0, 0, 'lifebar-fill', undefined, this);

    this._fillMask = this.game.add.graphics(9, 0, this);
    this._fillMask.beginFill(0xff00ff, 1);
    this._fillMask.drawRect(0, 0, 157, this._fill.height);
    this._fillMask.endFill();

    this._fill.mask = this._fillMask;
  }
}
