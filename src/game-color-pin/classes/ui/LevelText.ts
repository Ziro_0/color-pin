import GameState from '../../states/GameState';
import GameColors, { GameColorNames } from '../data/GameColors';

export default class LevelText extends Phaser.Text {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly FONT_STYLE: Phaser.PhaserTextStyle = {
    font: 'sunspire',
    fontSize: 48,
  };

  private static readonly Y = 40;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, viewOffset: number, group?: Phaser.Group) {
    const responsiveScale = (<GameState> game.state.getCurrentState()).responsiveScale;

    const style: Phaser.PhaserTextStyle = {
      font: LevelText.FONT_STYLE.font,
      fontSize: LevelText.FONT_STYLE.fontSize * responsiveScale,
    };

    super(
      game,
      game.world.centerX,
      0,
      '',
      style,
    );

    this.y = LevelText.Y * responsiveScale + viewOffset;

    this.anchor.x = 0.5;

    if (group) {
      group.add(this);
    }
  }

  // ----------------------------------------------------------------------------------------------
  setLevel(level: number, color?: GameColors): void {
    this.text = level.toString(10);
    this.setColor(color);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private setColor(color: GameColors): void {
    if (color) {
      const colorValue = GameColorNames.get(color);
      this.fill = Phaser.Color.getWebRGB(colorValue);
    }
  }
}
