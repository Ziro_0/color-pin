export default class UiFlash extends Phaser.Graphics {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static readonly DEFAULT_FLASH_ON_DURATION = 25;
  
  static readonly DEFAULT_FLASH_OFF_DURATION = 50

  private _timer: Phaser.Timer;

  private _numFlashes: number;

  private _flashOnDuration: number;

  private _flashOffDuration: number;

  private _started: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, numFlashes: number,
    flashOnDuration = UiFlash.DEFAULT_FLASH_ON_DURATION,
    flashOffDuration = UiFlash.DEFAULT_FLASH_OFF_DURATION,
    color = 0xff0000, alpha = 0.4) {
    super(game);

    this._numFlashes = numFlashes;
    this._flashOnDuration = flashOnDuration;
    this._flashOffDuration = flashOffDuration;

    this.beginFill(color, alpha);
    this.drawRect(0, 0, game.width, game.height);
    this.endFill();

    this._timer = game.time.create();
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  static Create(game: Phaser.Game, numFlashes: number, group: Phaser.Group): UiFlash {
    const uiFlash = new UiFlash(game, numFlashes);
    group.add(uiFlash);
    return (uiFlash);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this._timer.destroy();
    super.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (!this._started) {
      this._started = true;
      this.on();
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private off(): void {
    this.visible = false;

    this._numFlashes -= 1;

    if (this._numFlashes <= 0) {
      this.destroy();
      return;
    }

    this._timer.add(this._flashOffDuration,
      () => {
        this.on();
      }, this);
  }

  // ----------------------------------------------------------------------------------------------
  private on(): void {
    this.visible = true;
    this._timer.add(this._flashOnDuration,
      () => {
        this.off();
      }, this);
  }
}
