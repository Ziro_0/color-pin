import { ILevelData, ITargetMotionCycle, PartSizeConfigurations } from '../../IColorPinGameConfig';
import GameColors, { GameColorNames } from '../data/GameColors';
import IColorPart, { ColorPartSizes } from '../data/IColorPart';
import DartsLauncher from '../entities/DartsLauncher';
import Target from '../entities/Target';

export default class LevelBuilder {
  // ==============================================================================================
  // private
  // ==============================================================================================
  private _dartsLauncher: DartsLauncher;

  private _target: Target;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(dartsLauncher: DartsLauncher, target: Target) {
    this._dartsLauncher = dartsLauncher;
    this._target = target;
  }

  // ----------------------------------------------------------------------------------------------
  build(levelData: ILevelData): void {
    this.setDartSpeed(levelData);
    this.setDartLength(levelData);
    this.setPermaDarts(levelData);
    this.setTargetMotion(levelData);

    const colorParts = this.setColorParts(levelData);
    this.loadDarts(levelData, colorParts);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private static BuildColorParts(partSizeConfiguration: PartSizeConfigurations,
    colors: GameColors[]): IColorPart[] {
    let colorParts: IColorPart[];

    switch (partSizeConfiguration) {
      case PartSizeConfigurations.FOUR_QUARTERS:
        colorParts = LevelBuilder.GetColorPartsFourQuarters(colors);
        break;

      case PartSizeConfigurations.ONF_HALF_TWO_QUARTERS:
        colorParts = LevelBuilder.GetColorPartsOneHalfTwoQuarters(colors);
        break;

      case PartSizeConfigurations.THREE_THIRDS:
        colorParts = LevelBuilder.GetColorPartsThreeThirds(colors);
        break;

      case PartSizeConfigurations.TWO_HALVES:
        colorParts = LevelBuilder.GetColorPartsTwoHalves(colors);
        break;

      default:
        const partSizeConfigurations = [
          PartSizeConfigurations.FOUR_QUARTERS,
          PartSizeConfigurations.ONF_HALF_TWO_QUARTERS,
          PartSizeConfigurations.THREE_THIRDS,
          PartSizeConfigurations.TWO_HALVES,
        ];

        return (LevelBuilder.BuildColorParts(
          Phaser.ArrayUtils.getRandomItem(partSizeConfigurations),
          colors
        ));
    }

    return (colorParts);
  }

  // ----------------------------------------------------------------------------------------------
  private static DefaultTargetMotions(targetMotions: ITargetMotionCycle[]): ITargetMotionCycle[] {
    if (!targetMotions) {
      targetMotions = [];
    }

    targetMotions.forEach((targetMotion) => {
      if (targetMotion.isClockWise === undefined) {
        targetMotion.isClockWise = Math.random() >= 0.5;
      }

      if (targetMotion.speed === undefined) {
        targetMotion.speed = 3500;
      }

      if (targetMotion.durationMs === undefined) {
        targetMotion.durationMs = 5000;
      }
    });

    return (targetMotions.concat());
  }

  // ----------------------------------------------------------------------------------------------
  private static GetColorPartsFourQuarters(colors: GameColors[]): IColorPart[] {
    const _colors = colors && colors.length ? colors : LevelBuilder.GetRandomColors(4);
    return (
      [
        {
          size: ColorPartSizes.QUARTER,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 0,
        },
        {
          size: ColorPartSizes.QUARTER,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 270,
        },
        {
          size: ColorPartSizes.QUARTER,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 180,
        },
        {
          size: ColorPartSizes.QUARTER,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 90,
        },
      ]
    );
  }

  // ----------------------------------------------------------------------------------------------
  private static GetColorPartsOneHalfTwoQuarters(colors: GameColors[]): IColorPart[] {
    const _colors = colors && colors.length ? colors : LevelBuilder.GetRandomColors(3);
    return (
      [
        {
          size: ColorPartSizes.HALF,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 0,
        },
        {
          size: ColorPartSizes.QUARTER,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 225,
        },
        {
          size: ColorPartSizes.QUARTER,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 135,
        },
      ]
    );
  }

  // ----------------------------------------------------------------------------------------------
  private static GetColorPartsTwoHalves(colors: GameColors[]): IColorPart[] {
    const _colors = colors && colors.length ? colors : LevelBuilder.GetRandomColors(2);
    return (
      [
        {
          size: ColorPartSizes.HALF,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 0,
        },
        {
          size: ColorPartSizes.HALF,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 180,
        },
      ]
    );
  }

  // ----------------------------------------------------------------------------------------------
  private static GetColorPartsThreeThirds(colors: GameColors[]): IColorPart[] {
    const _colors = colors && colors.length ? colors : LevelBuilder.GetRandomColors(3);

    return (
      [
        {
          size: ColorPartSizes.THIRD,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 0,
        },
        {
          size: ColorPartSizes.THIRD,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 120,
        },
        {
          size: ColorPartSizes.THIRD,
          color: LevelBuilder.GetNextColor(_colors),
          angle: 240,
        },
      ]
    );
  }

  // ----------------------------------------------------------------------------------------------
  private static GetNextColor(colorsInOut: GameColors[]): GameColors {
    const color = colorsInOut.shift();
    colorsInOut.push(color);
    return (color);
  }

  // ----------------------------------------------------------------------------------------------
  private static GetRandomColors(numColors: number): GameColors[] {
    const names = Array.from(GameColorNames.keys());
    const availableNames: GameColors[] = [];
    const maxNumSameColor = numColors - 1;
    for (let count = 0; count < maxNumSameColor; count += 1) {
      availableNames.push(...names);
    }

    const randomColors: GameColors[] = [];
    for (let count = 0; count < numColors; count += 1) {
      randomColors.push(Phaser.ArrayUtils.removeRandomItem(availableNames));
    }

    return (randomColors);
  }

  // ----------------------------------------------------------------------------------------------
  private loadDarts(levelData: ILevelData, colorParts: IColorPart[]): void {
    let numDartsPerColor = levelData.numDartsPerColor;
    if (numDartsPerColor === undefined || numDartsPerColor <= 0) {
      numDartsPerColor = 1;
    }

    const dartColors: GameColors[] = [];

    colorParts.forEach((colorPart) => {
      const colorsPerDart = new Array(numDartsPerColor).fill(colorPart.color);
      dartColors.push(...colorsPerDart);
    });

    Phaser.ArrayUtils.shuffle(dartColors);
    this._dartsLauncher.loadDarts(dartColors);
  }

  // ----------------------------------------------------------------------------------------------
  private setPermaDarts(levelData: ILevelData): void {
    const permaDartAngles = levelData.permaDartAngles;
    if (!permaDartAngles) {
      return;
    }

    permaDartAngles.forEach((permaDartAngle) => {
      this._target.attachPermaDart(permaDartAngle);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private setColorParts(levelData: ILevelData): IColorPart[] {
    const colorParts = LevelBuilder.BuildColorParts(
      levelData.partSizeConfiguration,
      levelData.partColors
    );

    this._target.setColorParts(colorParts);
    return (colorParts);
  }

  // ----------------------------------------------------------------------------------------------
  private setDartLength(levelData: ILevelData): void {
    this._dartsLauncher.stemLength = levelData.stemLength;
    this._target.permaDartStemLength = levelData.stemLength;
  }

  // ----------------------------------------------------------------------------------------------
  private setDartSpeed(levelData: ILevelData): void {
    let dartSpeedMs = levelData.dartSpeedMs;
    if (dartSpeedMs === undefined || dartSpeedMs < 0) {
      dartSpeedMs = 150;
    }

    this._dartsLauncher.fireDuration = dartSpeedMs;
  }

  // ----------------------------------------------------------------------------------------------
  private setTargetMotion(levelData: ILevelData): void {
    let targetMotions = levelData.targetMotions;
    targetMotions = LevelBuilder.DefaultTargetMotions(targetMotions);
    this._target.angle = Math.random() * 360;
    this._target.setMotion(targetMotions);
  }
}
