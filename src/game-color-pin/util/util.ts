// ================================================================================================
// CONSTS, INTERFACES, TYPES
// ================================================================================================

/**
 * Defines a 2D point.
 */
 export interface IPoint {
  /**
   * The x coordinate of the point.
   */
  x?: number;
  
  /**
   * The y coordinate of the point.
   */
  y?: number;
}

// ------------------------------------------------------------------------------------------------
/**
 * Defines a 2D rectangle.
 */
 export interface IRect extends IPoint {
  /**
   * The width of the rectangle.
   */
  width?: number;
  
  /**
   * The height of the rectangle.
   */
  height?: number;
}

// ================================================================================================
// FUNCTIONS
// ================================================================================================
export function choose(...items: any[]): any {
  if (items.length === 0) {
    throw new Error('You need to pass at least one item');
  }
  return (items[irandom(items.length - 1)]);
}

// ------------------------------------------------------------------------------------------------
export function chooseWeights(weights: number[]): number {
  const length = weights ? weights.length : 0;
  if (length === 0) {
    return (-1);
  }
  
  let sumOfWeights = 0;
  weights.forEach((weight) => {
    sumOfWeights += weight;
  });
  
  const threshold = Math.random() * sumOfWeights;
  let test = 0;
  for (let index = 0; index < length - 1; index += 1) {
    test += weights[index];
    if (test >= threshold) {
      return (index);
    }
  }  

  return (length - 1); //random number was in range of the last index
}

// ------------------------------------------------------------------------------------------------
export function checkOverlap(spriteA: PIXI.Sprite, spriteB: PIXI.Sprite): boolean {
  const boundsA = new Phaser.Rectangle().copyFromBounds(spriteA.getBounds());
  const boundsB = new Phaser.Rectangle().copyFromBounds(spriteB.getBounds());
  return (Phaser.Rectangle.intersects(boundsA, boundsB));
}


// ------------------------------------------------------------------------------------------------
export function getProperty(source: object, name: string, defaultValue?: any): any {
  if (!source || !name) {
    return (defaultValue);
  }

  let keys = name.split('.');
  const len = keys.length;
  let i = 0;
  let val = source;

  while (i < len) {
    const key = keys[i];

    if (val !== null && val !== undefined) {
      val = val[key];
      i += 1;
    } else {
      return (defaultValue);
    }
  }

  return (val !== null && val !== undefined ? val : defaultValue);
}

// ------------------------------------------------------------------------------------------------
export function getBooleanProperty(source: object, name: string,
  defaultValue?: boolean): boolean {
  return (getProperty(source, name, defaultValue));
}

// ------------------------------------------------------------------------------------------------
export function getNumberProperty(source: object, name: string, defaultValue?: number): number {
  return (getProperty(source, name, defaultValue));
}

// ------------------------------------------------------------------------------------------------
export function getStringProperty(source: object, name: string, defaultValue?: string): string {
  return (getProperty(source, name, defaultValue));
}

// ------------------------------------------------------------------------------------------------
export function irandom(max: number): number {
  return (Math.floor(Math.random() * (max + 1)));
}

// ------------------------------------------------------------------------------------------------
export function irandomRange(min: number, max: number): number {
  return (Math.floor(Math.random() * (max - min + 1)) + min);
}

// ------------------------------------------------------------------------------------------------
export function isMobile(game: Phaser.Game): boolean {
  const device = game.device;
  return (device.android || device.iOS || device.windowsPhone || device.touch);
}

// ------------------------------------------------------------------------------------------------
export function randomRange(min: number, max: number): number {
  return (Math.random() * (max - min) + min);
}

// ------------------------------------------------------------------------------------------------
export function removeTimer(timerEvent: Phaser.TimerEvent): Phaser.TimerEvent {
  if (timerEvent) {
    timerEvent.timer.remove(timerEvent);
  }

  return (null);
}
