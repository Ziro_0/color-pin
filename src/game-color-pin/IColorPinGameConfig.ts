import GameColors from './classes/data/GameColors';

// ================================================================================================
/**
 * Defines how the target circle should move while the player is on this leevl.
 * 
 * For example:
 * 
 * Say `targetMosions` is defined like this:
 * 
 * ```
 * [
 *   {
 *     speed: 3000,
 *     isClockWise: true,
 *     durationMs: 6000,
 *   },
 * 
 *   {
 *     speed: 3500,
 *     isClockWise: false,
 *     durationMs: 7000,
 *   },
 * ]
 * ```
 * In the firset element, the target circle will spin clockwise, at 3000 ms per revolusions, for
 * six seconds. Then, it'll switch counter-clockwise, changing speeds to 3500 ms, which lasts for
 * seven seconds. Afterwards, the target circle starts over with the first element.
 */
export interface ITargetMotionCycle {
  /**
   * Specifies the duration, in ms, it takes the target circle to make one complete 360-degree
   * turn.
   * 
   * Optional, and defaults to `3500`.
   */
  speed?: number;

  /**
   * If the target circle cycles through multiple motions, this specifies the duration, in
   * ms, of this cycle before moving to the next cycle.
   * 
   * Optional, and default to `5000` if not specified. Not really needed if a level only
   * has one motion.
   */
  durationMs?: number;

  /**
   * Specifies if the target circle spins clockwise (`true`) or counter-clockwise (`fasle`).
   * 
   * Optional, and will be random if not specified.
   */
  isClockWise?: boolean;
}

// ================================================================================================
export enum PartSizeConfigurations {
  /**
   * The target circle is filled with (4) quarter parts.
   */
  FOUR_QUARTERS = 'fourQuarters',

  /**
   * The target circle is filled with (1) half part, and (2) quarter parts.
   */
  ONF_HALF_TWO_QUARTERS = 'oneHalfTwoQuarters',

  /**
   * The target circle is filled with (3) third parts.
   */
  THREE_THIRDS = 'threeThirds',

  /**
   * The target circle is filled with (2) half parts.
   */
  TWO_HALVES = 'twoHalves',
}

// ================================================================================================
/**
 * Data describing each level.
 */
export interface ILevelData {
  /**
   * Amount of time, in ms, it takes for a fired dart to reach the target.
   * 
   * Optional, and defaults to `150`.
   */
  dartSpeedMs?: number;

  /**
   * Specifies if an previously, successfully thrown dart can be broken if a new dart collides with
   * it. A broken dart will be removed from the target circle, and you'll need to successfully throw
   * it again.
   */
  canBreakExistingDarts?: boolean;

  /**
   * Number of darts assigned to each part.
   * 
   * Optional, and defaults to one color per part.
   */
  numDartsPerColor?: number;

  /**
   * Specifies the color of each part. Parts are defined in the order of the colors specified. Be sure to
   * specify `GameColors` enum values, such as `GameColors.BLUE`, and _not_ `'blue'`.
   * 
   * Optional, and defaults to random colors.
   */
  partColors?: GameColors[];

  /**
   * Defines which sizes of parts will fill the target circle.
   * 
   * Optional, and defaults to a random configuration being chosen if not specified.
   * @see PartSizeConfigurations
   */
  partSizeConfiguration?: PartSizeConfigurations;

  /**
   * Angles, in degrees, of any permanent darts that are on the level. Angles start at 0 (right),
   * and go clockwise to 360. Permanent darts are always black, and cannot be removed.
   */
  permaDartAngles?: number[];

  /**
   * Number of points added to your score for each successful dart thrown on this level.
   * 
   * Optional, and defaults to `1`.
   */
  pointsPerHit?: number;

  /**
   * Number of points subtracted from your score for each missed dart thrown on this level.
   * 
   * Optional, and defaults to `5`.
   */
  pointsPerMiss?: number;

   /**
   * Length of the stem of each dart on this level.
   * 
   * Optional, and defaults to the width of the 'dart-stem' image used to draw the stem.
   */
  stemLength?: number;

  /**
   * Defines how the target circle moves.
   * 
   * @see ITargetMotionCycle
   */
  targetMotions?: ITargetMotionCycle[];
}

// ================================================================================================
/**
 * Specifies game config properties for "color-pin" game.
 */
export default interface IColorPinGameConfig {
  /**
   * Game start time, in seconds. Required.
   */
  startTimeInSeconds: number;

   /**
   * Array of level data describing each level. Required.
   * 
   * Each time the player completes a level, they will proceed to the next one. If the player wins
   * the last level, the game stays on that level (unless a level penalty says otherwise).
   * 
   * You can define as many levels as you want, but there must be at least one level defined.
   * 
   * See `ILevelData` for more details.
   */
  levels: ILevelData[];

  /**
   * Number of consecutive successful dart hits (without a miss) to be awarded another heart.
   * For each dart hit, hearts multiple the points awarded X the current number of hearts.
   * 
   * Optional, and defaults to `15`.
   */
  numConsecutiveHitsForHeartsBonus?: number;

  /**
   * Sets the level on which the player starts. Useful for debugging later levels.
   * 
   * Optional, and defaults to `0`.
   */
  startLevel?: number;
}
